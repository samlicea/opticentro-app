﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

// Another method of using a Database, could not implement or use

namespace OpticentroShell.Services
{
    public interface ISQLite
    {
        SQLiteConnection GetConnection();
    }
}
