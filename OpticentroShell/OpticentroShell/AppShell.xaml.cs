﻿using OpticentroShell.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OpticentroShell
{
    public partial class AppShell : Shell
    {
        public AppShell()
        {
            InitializeComponent();
            RegisterRoutes();
        }

        // Registers routes for navigating pages
        private void RegisterRoutes()
        {
            Routing.RegisterRoute("AboutPage", typeof(NewFrameEntry));
            Routing.RegisterRoute("FramesPage", typeof(FramesPage));
        }
    }
}
