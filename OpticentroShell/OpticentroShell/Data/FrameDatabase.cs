﻿using OpticentroShell.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

/*Implementation of commands from the Database, working*/

namespace OpticentroShell.Data
{
    public class FrameDatabase
    {
        readonly SQLiteAsyncConnection _database;

        public FrameDatabase(string dbPath)
        {
            _database = new SQLiteAsyncConnection(dbPath);
            _database.CreateTableAsync<Frame>().Wait();
        }

        public Task<List<Frame>> GetFramesAsync()
        {
            return _database.Table<Frame>().ToListAsync();
        }

        public Task<Frame> GetFrameAsync(int id)
        {
            return _database.Table<Frame>().Where(i => i.ID == id).FirstOrDefaultAsync();
        }

        public Task<int> SaveFrameAsync(Frame frame)
        {
            if(frame.ID != 0)
            {
                return _database.UpdateAsync(frame);
            }
            else
            {
                return _database.InsertAsync(frame);
            }

        }

        public Task<int> DeleteFrameAsync(Frame frame)
        {
            return _database.DeleteAsync(frame);
        }
    }
}
