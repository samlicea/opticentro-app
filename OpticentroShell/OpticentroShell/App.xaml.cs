﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using OpticentroShell.Services;
using OpticentroShell.Views;
using System.IO;
using OpticentroShell.Data;

namespace OpticentroShell
{
    public partial class App : Application
    {
        //public static string FilePath;

        static FrameDatabase database;

        // Database implementation, gets location of where to store local DB
        public static FrameDatabase Database
        {
            get
            {
                if (database == null)
                {
                    database = new FrameDatabase(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Frames.db3"));
                }
                return database;
            }
        }

        public App()
        {
            InitializeComponent();

            MainPage = new AppShell();

        }

        ////New instance receiving the DB filepath for android
        //public App(string filePath)
        //{
        //    InitializeComponent();

        //    MainPage = new AppShell();

        //    FilePath = filePath;

        //}

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
