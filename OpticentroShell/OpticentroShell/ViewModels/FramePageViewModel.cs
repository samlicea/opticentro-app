﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

/*Tried to implement the Data in the ViewModel, which is the correct way of doing MVVM, but could not figure out how to connect it to my ViewPages*/

namespace OpticentroShell.ViewModels
{
    //public frames ObservableCollection<Model.Frame> { get; set; }

    public class FramePageViewModel
    {

        public ObservableCollection<Models.Frame> FramesCollection { get; set; }

        public FramePageViewModel()
        {
            FramesCollection = new ObservableCollection<Models.Frame>()
            {
                    new Models.Frame { Image = "apt1.png", StoreLocation = "Opticentro Soler", PriceRange = "$$", ModelName = "H-P17 - Guess",
                        Material = "Frameless", Gender = "Men's", Color = "Black",
                        Details = "Details about frame" },
                    new Models.Frame { Image = "apt2.png", StoreLocation = "Optica Colonial", PriceRange = "$", ModelName = "1827827",
                        Material = "Full Frame", Gender = "Unisex", Color = "Blue",
                        Details = "Details here!" },
                    new Models.Frame { Image = "apt3.png", StoreLocation = "Optica Colonial", PriceRange = "$$$", ModelName = "111222",
                        Material = "Pasta", Gender = "Women's", Color = "Transparent",
                        Details = "Details again go here" },
            };


        }

    }
}
