﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpticentroShell.Models
{
    // Product used in first page
    public class Product
    {
        public string Model { get; set; }
        public string PriceRange { get; set; }
        public string Image { get; set; }
    }
}
