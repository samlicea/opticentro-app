﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpticentroShell.Models
{
    // Used to create services in the ServicesPage

    public class OfferedService
    {
        public string Image { get; set; }
        public string Title { get; set; }
    }
}
