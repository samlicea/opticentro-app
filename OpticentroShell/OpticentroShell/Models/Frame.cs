﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpticentroShell.Models
{
    [Table("Frames")]
    public class Frame
    {
        // ID used in DB file
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        //public string Id => Guid.NewGuid().ToString("N");
        public string ModelName { get; set; }
        public string Image { get; set; }
        public string StoreLocation { get; set; }

        [MaxLength(4)]
        public string PriceRange { get; set; }
        public string Material { get; set; }
        public string Gender { get; set; }
        public string Color { get; set; }
        public string Details { get; set; }
    }
}
