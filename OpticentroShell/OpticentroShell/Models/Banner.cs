﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpticentroShell.Models
{
    // Model for banner, used in first page
    public class Banner
    {
        public string Heading { get; set; }
        public string Message { get; set; }
        public string Caption { get; set; }
        public string Image { get; set; }
    }
}
