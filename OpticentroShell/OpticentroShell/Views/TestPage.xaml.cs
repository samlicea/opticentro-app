﻿using OpticentroShell.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

/*Test page used to see if writing to Database works if using a listView*/
namespace OpticentroShell.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TestPage : ContentPage
    {
        public TestPage()
        {
            InitializeComponent();
        }

        // Populates list with frames
        protected override async void OnAppearing()
        {
            base.OnAppearing();

            framesList.ItemsSource = await App.Database.GetFramesAsync();
        }

        private async void framesList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            //var details = e.Item as Frame;

            var frame = (sender as View).BindingContext as Models.Frame;
            await this.Navigation.PushAsync(new DetailsPage(frame));
        }
    }
}