﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OpticentroShell.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FullFrameFramePage : ContentPage
    {
        public FullFrameFramePage()
        {
            InitializeComponent();
            this.BindingContext = this;
        }



        // List of all the frames in stock
        public List<Models.Frame> FramesList => GetFrames();

        private List<Models.Frame> GetFrames()
        {
            return new List<Models.Frame>
            {

                    // Full Frame

                    new Models.Frame {
                        Image = "ff.png",
                        StoreLocation = "Opticentro Soler",
                        PriceRange = "$$",
                        ModelName = "FullFrame-001-Black",
                        Material = "Full Frame",
                        Gender = "Unisex",
                        Color = "Black",
                        Details = "NA"
                    },

                    new Models.Frame {
                        Image = "ff1.png",
                        StoreLocation = "Opticentro Soler",
                        PriceRange = "$$$",
                        ModelName = "FullFrame-002-Blue",
                        Material = "Full Frame",
                        Gender = "Unisex",
                        Color = "Blue",
                        Details = "NA"
                    },

                    new Models.Frame {
                        Image = "ff2.png",
                        StoreLocation = "Opticentro Soler",
                        PriceRange = "$",
                        ModelName = "FullFrame-003-Black/Blue",
                        Material = "Full Frame",
                        Gender = "Unisex",
                        Color = "Black/Blue",
                        Details = "Perfect blue details on the inside, black exterior."
                    },

                    new Models.Frame {
                        Image = "ff3.png",
                        StoreLocation = "Opticentro Soler",
                        PriceRange = "$$",
                        ModelName = "FullFrame-004-Red",
                        Material = "Full Frame",
                        Gender = "Unisex",
                        Color = "Red",
                        Details = "NA"
                    },

                   
            };
        }

        // Opens the individual frame in details page - FrameSelected
        private async void FrameSelected(object sender, EventArgs e)
        {
            var property = (sender as View).BindingContext as Models.Frame;
            await this.Navigation.PushAsync(new DetailsPage(property));
        }


        // User searches for frames in catalog of frames
        private void SearchResult(object sender, EventArgs e)
        {
            // Needs implementation of search
        }

        // Need to use with Collection in order to change frame details
        private async void AddFrameButton(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync("AboutPage");
        }

        private async void RemoveFrameButton(object sender, EventArgs e)
        {
            await Shell.Current.Navigation.PopAsync(animated: false);
        }
    }
}