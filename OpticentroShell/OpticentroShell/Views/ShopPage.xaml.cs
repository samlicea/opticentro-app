﻿using OpticentroShell.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OpticentroShell.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ShopPage : ContentPage
    {
        public ShopPage()
        {
            InitializeComponent();
            this.BindingContext = this;

        }

        public List<Banner> Banners { get => GetBanners(); }
        public List<Product> NewGlassesList { get => GetNewGlasses(); }
        public List<Product> SpecialsList { get => GetSpecials(); }

        //Creates three different lists to fill info for page
        private List<Banner> GetBanners()
        {
            var bannerList = new List<Banner>
            {
                //Add custom banners, examples below

                new Banner { Heading = "VINTAGE COLLECTION", Message = "NEW MODELS", Caption = "BETTER MODELS", Image = "banner1.jpg" },
                new Banner { Heading = "SUNGLASSES COLLECTION", Message = "UP TO 10% OFF", Caption = "See Deals Below", Image = "banner2.jpg" },
                new Banner { Heading = "SUMMER COLLECTION", Message = "20% Discount", Caption = "EARLY SUMMER SALE", Image = "banner4.jpg" }
            };
            return bannerList;
        }

        private List<Product> GetNewGlasses()
        {
            var newGlasses = new List<Product>
            {

                // Add products here
                new Product { Image = "ff.jpg", Model = "FullFrame-001-Black", PriceRange = "$$" },
                new Product { Image = "lb.jpg", Model = "LB-001-Tran", PriceRange = "$$$" },
                new Product { Image = "rims.jpg", Model = "Rim-001-Black", PriceRange = "$$$" }
            };

            return newGlasses;

        }

        private List<Product> GetSpecials()
        {
            var newSpecials = new List<Product>
            {

                // Add products here
                new Product { Image = "srim1.jpg", Model = "Srim-002-Cream", PriceRange = "20% OFF" },
                new Product { Image = "srim2.jpg", Model = "Srim-003-Black", PriceRange = "50% OFF" }
            };

            return newSpecials;

        }


        // Shows all the glasses in that category
        private void ShowAllButton(object sender, EventArgs e)
        {
            // Need to implement List view with custom cells
        }

        // Takes you to detailed item (DetailsPage)
        private void ImageClicked(object sender, EventArgs e)
        {
            // Pushes to new page and passes information to that page
        }
    }
}