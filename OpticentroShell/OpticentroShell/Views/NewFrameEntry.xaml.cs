﻿using OpticentroShell.Models;
using SQLite;
using System;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

/*This page was not used in the end, it was meant to be used as an entry page to create a new frame product in the DB. Could not figure out how to read from DB in the shopPage
 * Reads and writes from DB correctly*/


namespace OpticentroShell.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class NewFrameEntry : ContentPage
    {
        public NewFrameEntry()
        {
            InitializeComponent();
        }

        private async void AddFrametoDB(object sender, EventArgs e)
        {
            //var db = new SQLiteConnection(App.FilePath);
            //db.CreateTable<Models.Frame>();

            //var maxPK = db.Table<Models.Frame>().OrderByDescending(c => c.Id).FirstOrDefault();


            Models.Frame frame = new Models.Frame()
            {
                ModelName = FrameModelName.Text,
                StoreLocation = FrameStoreLocation.Text,
                PriceRange = FramePriceRange.Text,
                Material = FrameMaterial.Text,
                Gender = FrameGender.Text,
                Color = FrameColor.Text,
                Details = FrameDetails.Text
            };

            await App.Database.SaveFrameAsync(frame);

            FrameModelName.Text = string.Empty;
            FrameStoreLocation.Text = string.Empty;
            FramePriceRange.Text = string.Empty;
            FrameMaterial.Text = string.Empty;
            FrameGender.Text = string.Empty;
            FrameColor.Text = string.Empty;
            FrameDetails.Text = string.Empty;

            await DisplayAlert("Success", "Frame added to DB", "ok");
            var FramesList = await App.Database.GetFramesAsync();

            //if(FramesList != null)
            //{
            //    FramesList.ItemsSource = FramesList;
            //}

            //var frame = (Models.Frame)BindingContext;

            //frame.ModelName = FrameModelName.Text;
            //frame.StoreLocation = FrameStoreLocation.Text;
            //frame.PriceRange = FramePriceRange.Text;
            //frame.Material = FrameMaterial.Text;
            //frame.Gender = FrameGender.Text;
            //frame.Color = FrameColor.Text;
            //frame.Details = FrameDetails.Text;


            //_ = await App.Database.SaveFrameAsync(frame);
            ////await Navigation.PopAsync();
            await Shell.Current.Navigation.PopAsync(animated: false);
            //await Shell.Current.GoToAsync("FramesPage");


            //    Models.Frame frame = new Models.Frame()
            //    {
            //        ModelName = FrameModelName.Text,
            //        StoreLocation = FrameStoreLocation.Text,
            //        PriceRange = FramePriceRange.Text,
            //        Material = FrameMaterial.Text,
            //        Gender = FrameGender.Text,
            //        Color = FrameColor.Text,
            //        Details = FrameDetails.Text
            //    };

            //    using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
            //    {
            //        conn.CreateTable<Models.Frame>();
            //        int rowsAdded = conn.Insert(frame);
            //    }

            //    // Currently goes back to Framespage, need to show message "Successful"
            //    Application.Current.MainPage = new FramesPage();
            //}
        }
    }
}