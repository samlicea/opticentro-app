﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OpticentroShell.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LowBridgeFramePage : ContentPage
    {
        public LowBridgeFramePage()
        {
            InitializeComponent();
            this.BindingContext = this;
        }

        // List of all the frames in stock
        public List<Models.Frame> FramesList => GetFrames();

        private List<Models.Frame> GetFrames()
        {
            return new List<Models.Frame>
            {

                    // Low-Bridge 

                    new Models.Frame {
                        Image = "lb.png",
                        StoreLocation = "Optica Colonial",
                        PriceRange = "$$$",
                        ModelName = "LB-001-Tran",
                        Material = "Low Bridge",
                        Gender = "Unisex",
                        Color = "Transparent",
                        Details = "Perfect for Hipster and young people"
                    },

                    new Models.Frame {
                        Image = "lb1.png",
                        StoreLocation = "Optica Colonial",
                        PriceRange = "$",
                        ModelName = "LB-002-Rose",
                        Material = "Low Bridge",
                        Gender = "Women's",
                        Color = "Rose",
                        Details = "Medium to larger size, ideal for people with medium-size faces."
                    },

                    new Models.Frame {
                        Image = "lb2.png",
                        StoreLocation = "Opticentro Soler",
                        PriceRange = "$$$",
                        ModelName = "LB-003-RoseTran",
                        Material = "Low Bridge",
                        Gender = "Women's",
                        Color = "Transparent/Rose",
                        Details = "Designed in Italy, vintage look"
                    },

            };
        }

        // Opens the individual frame in details page - FrameSelected
        private async void FrameSelected(object sender, EventArgs e)
        {
            var property = (sender as View).BindingContext as Models.Frame;
            await this.Navigation.PushAsync(new DetailsPage(property));
        }

        // User searches for frames in catalog of frames
        private void SearchResult(object sender, EventArgs e)
        {
            // Needs implementation of search
        }

        // Need to use with Collection in order to change frame details
        private async void AddFrameButton(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync("AboutPage");
        }

        private async void RemoveFrameButton(object sender, EventArgs e)
        {
            await Shell.Current.Navigation.PopAsync(animated: false);
        }

    }
}