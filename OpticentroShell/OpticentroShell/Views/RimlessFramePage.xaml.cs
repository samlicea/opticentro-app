﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OpticentroShell.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RimlessFramePage : ContentPage
    {
        public RimlessFramePage()
        {
            InitializeComponent();
            this.BindingContext = this;
        }



        // List of all the frames in stock
        public List<Models.Frame> FramesList => GetFrames();

        private List<Models.Frame> GetFrames()
        {
            return new List<Models.Frame>
            {
                    // Rimless
                    new Models.Frame {
                        Image = "rims.png",
                        StoreLocation = "Opticentro Soler",
                        PriceRange = "$$$",
                        ModelName = "Rim-001-Black",
                        Material = "Rimless",
                        Gender = "Men's",
                        Color = "Black",
                        Details = "Designed in Italy"
                    },

                    new Models.Frame
                    {
                        Image = "rims1.png",
                        StoreLocation = "Opticentro Soler",
                        PriceRange = "$$",
                        ModelName = "Rim-002-Gray",
                        Material = "Rimless",
                        Gender = "Men's",
                        Color = "Gray",
                        Details = "This frame was worn by .... "
                    },

                    new Models.Frame
                    {
                        Image = "rims2.png",
                        StoreLocation = "Optica Colonial",
                        PriceRange = "$$",
                        ModelName = "Rim-003-Metal",
                        Material = "Rimless",
                        Gender = "Unisex",
                        Color = "Metal",
                        Details = "NA"
                    },

 

            };
        }

        // Opens the individual frame in details page - FrameSelected
        private async void FrameSelected(object sender, EventArgs e)
        {
            var property = (sender as View).BindingContext as Models.Frame;
            await this.Navigation.PushAsync(new DetailsPage(property));
        }

        // User searches for frames in catalog of frames
        private void SearchResult(object sender, EventArgs e)
        {
            // Needs implementation of search
        }

        // Need to use with Collection in order to change frame details
        private async void AddFrameButton(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync("AboutPage");
        }

        private async void RemoveFrameButton(object sender, EventArgs e)
        {
            await Shell.Current.Navigation.PopAsync(animated: false);
        }


    }
}