﻿using OpticentroShell.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OpticentroShell.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ServicesPage : ContentPage
    {
        public ServicesPage()
        {
            InitializeComponent();
            this.BindingContext = this;
        }


        // List that holds services
        public List<OfferedService> InStoreServices { get => GetServices(); }

        // Creates a list of services
        private List<OfferedService> GetServices()
        {
            var ServicesList = new List<OfferedService>
            {
                // Add services here
                new OfferedService { Image = "service1.jpg", Title = "PROFESSIONAL EYE EXAMS" },
                new OfferedService { Image = "service2.jpg", Title = "FRAMES REPAIR" },
                new OfferedService { Image = "service3.jpg", Title = "CONTACT LENSES" },
                new OfferedService { Image = "service4.jpg", Title = "ACCESSORIES" }
            };
            return ServicesList;
        }
    }
}