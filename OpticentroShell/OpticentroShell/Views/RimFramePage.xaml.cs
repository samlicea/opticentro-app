﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OpticentroShell.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RimFramePage : ContentPage
    {
        public RimFramePage()
        {
            InitializeComponent();
            this.BindingContext = this;
        }



        // List of all the frames in stock
        public List<Models.Frame> FramesList => GetFrames();

        private List<Models.Frame> GetFrames()
        {
            return new List<Models.Frame>
            {
                    // Semi-Rimmless
                    new Models.Frame {
                        Image = "srim.png",
                        StoreLocation = "Opticentro Soler",
                        PriceRange = "$$$",
                        ModelName = "Srim-001-Metal",
                        Material = "Semi-Rimless",
                        Gender = "Men's",
                        Color = "Metal",
                        Details = "Designed in Mexico"
                    },

                    new Models.Frame {
                        Image = "srim1.png",
                        StoreLocation = "Opticentro Soler",
                        PriceRange = "$$$$",
                        ModelName = "Srim-002-Cream",
                        Material = "Semi-Rimless",
                        Gender = "Women's",
                        Color = "Cream",
                        Details = "Designed in Mexico"
                    },

                    new Models.Frame {
                        Image = "srim2.png",
                        StoreLocation = "Optica Colonial",
                        PriceRange = "$$",
                        ModelName = "Srim-003-Black",
                        Material = "Semi-Rimless",
                        Gender = "Men's",
                        Color = "Black",
                        Details = "Very thin, looks good on people with slim faces"
                    },

                    new Models.Frame {
                        Image = "srim3.png",
                        StoreLocation = "Optica Colonial",
                        PriceRange = "$$$$",
                        ModelName = "Srim-004-Wood-Gold",
                        Material = "Semi-Rimless",
                        Gender = "Women's",
                        Color = "Wood/Gold",
                        Details = "High end with Gold details"
                    },

              
            };
        }


        // Opens the individual frame in details page - FrameSelected
        private async void FrameSelected(object sender, EventArgs e)
        {
            var property = (sender as View).BindingContext as Models.Frame;
            await this.Navigation.PushAsync(new DetailsPage(property));
        }

        // User searches for frames in catalog of frames
        private void SearchResult(object sender, EventArgs e)
        {
            // Needs implementation of search
        }

        // Need to use with Collection in order to change frame details
        private async void AddFrameButton(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync("AboutPage");
        }

        private async void RemoveFrameButton(object sender, EventArgs e)
        {
            await Shell.Current.Navigation.PopAsync(animated: false);
        }
    }
}