﻿using OpticentroShell.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OpticentroShell.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailsPage : ContentPage
    {
        public Models.Frame FrameDetail { get; }

        public DetailsPage(Models.Frame FrameDetail)
        {
            InitializeComponent();

            // Used for database information passing between pages

            //storelocation.Text = FrameDetail.StoreLocation;
            //pricerange1.Text = FrameDetail.PriceRange;
            //material.Text = FrameDetail.Material;
            //gender.Text = FrameDetail.Gender;
            //color.Text = FrameDetail.Color;
            //pricerange.Text = FrameDetail.PriceRange;
            //modelname.Text = FrameDetail.ModelName;
            //details.Text = FrameDetail.Details;

            this.FrameDetail = FrameDetail;
            this.BindingContext = this;
        }


        private void GoBack(object sender, EventArgs e)
        {
            Application.Current.MainPage = new FramesPage();
            //this.Navigation.PopAsync();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            DetailsView.TranslationY = 600;
            DetailsView.TranslateTo(0, 0, 500, Easing.SinInOut);
        }

        // Meant to be used as a reseve button for employees, could not figure out how to write to DB and remove from the listing
        private async void ReserveButtonClicked(object sender, EventArgs e)
        {
            // Implement a page that confirms reservation, gives pickup location, and has button to continue shopping
            await DisplayAlert("Success", "Frame was reserved. When you arrive please tell us your name", "OK");
            await Shell.Current.Navigation.PopAsync(animated: true);
        }
    }
}