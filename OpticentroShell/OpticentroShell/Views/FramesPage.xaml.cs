﻿using OpticentroShell.Data;
using OpticentroShell.Models;
using OpticentroShell.ViewModels;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OpticentroShell.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FramesPage : ContentPage
    {
        //private readonly FramePageViewModel _collection = new FramePageViewModel();

        public FramesPage()
        {
            InitializeComponent();
            this.BindingContext = this;
            //BindingContext = _collection;
        }


        // List of all the frames in stock
        public List<Models.Frame> FramesList => GetFrames();

        private List<Models.Frame> GetFrames()
        {
            return new List<Models.Frame>
            {
                    // Rimless
                    new Models.Frame { 
                        Image = "rims.png", 
                        StoreLocation = "Opticentro Soler", 
                        PriceRange = "$$$", 
                        ModelName = "Rim-001-Black",
                        Material = "Rimless", 
                        Gender = "Men's", 
                        Color = "Black",
                        Details = "Designed in Italy" 
                    },

                    new Models.Frame
                    {
                        Image = "rims1.png",
                        StoreLocation = "Opticentro Soler",
                        PriceRange = "$$",
                        ModelName = "Rim-002-Gray",
                        Material = "Rimless",
                        Gender = "Men's", 
                        Color = "Gray",
                        Details = "This frame was worn by .... "
                    },

                    new Models.Frame
                    {
                        Image = "rims2.png",
                        StoreLocation = "Optica Colonial",
                        PriceRange = "$$",
                        ModelName = "Rim-003-Metal",
                        Material = "Rimless",
                        Gender = "Unisex", 
                        Color = "Metal",
                        Details = "NA"
                    },

                    // Semi-Rimmless
                    new Models.Frame {
                        Image = "srim.png",
                        StoreLocation = "Opticentro Soler",
                        PriceRange = "$$$",
                        ModelName = "Srim-001-Metal",
                        Material = "Semi-Rimless",
                        Gender = "Men's",
                        Color = "Metal",
                        Details = "Designed in Mexico"
                    },

                    new Models.Frame {
                        Image = "srim1.png",
                        StoreLocation = "Opticentro Soler",
                        PriceRange = "$$$$",
                        ModelName = "Srim-002-Cream",
                        Material = "Semi-Rimless",
                        Gender = "Women's",
                        Color = "Cream",
                        Details = "Designed in Mexico"
                    },

                    new Models.Frame {
                        Image = "srim2.png",
                        StoreLocation = "Optica Colonial",
                        PriceRange = "$$",
                        ModelName = "Srim-003-Black",
                        Material = "Semi-Rimless",
                        Gender = "Men's",
                        Color = "Black",
                        Details = "Very thin, looks good on people with slim faces"
                    },

                    new Models.Frame {
                        Image = "srim3.png",
                        StoreLocation = "Optica Colonial",
                        PriceRange = "$$$$",
                        ModelName = "Srim-004-Wood-Gold",
                        Material = "Semi-Rimless",
                        Gender = "Women's",
                        Color = "Wood/Gold",
                        Details = "High end with Gold details"
                    },

                    // Full Frame

                    new Models.Frame {
                        Image = "ff.png",
                        StoreLocation = "Opticentro Soler",
                        PriceRange = "$$",
                        ModelName = "FullFrame-001-Black",
                        Material = "Full Frame",
                        Gender = "Unisex",
                        Color = "Black",
                        Details = "NA"
                    },

                    new Models.Frame {
                        Image = "ff1.png",
                        StoreLocation = "Opticentro Soler",
                        PriceRange = "$$$",
                        ModelName = "FullFrame-002-Blue",
                        Material = "Full Frame",
                        Gender = "Unisex",
                        Color = "Blue",
                        Details = "NA"
                    },

                    new Models.Frame {
                        Image = "ff2.png",
                        StoreLocation = "Opticentro Soler",
                        PriceRange = "$",
                        ModelName = "FullFrame-003-Black/Blue",
                        Material = "Full Frame",
                        Gender = "Unisex",
                        Color = "Black/Blue",
                        Details = "Perfect blue details on the inside, black exterior."
                    },

                    new Models.Frame {
                        Image = "ff3.png",
                        StoreLocation = "Opticentro Soler",
                        PriceRange = "$$",
                        ModelName = "FullFrame-004-Red",
                        Material = "Full Frame",
                        Gender = "Unisex",
                        Color = "Red",
                        Details = "NA"
                    },

                    // Low-Bridge 

                    new Models.Frame {
                        Image = "lb.png",
                        StoreLocation = "Optica Colonial",
                        PriceRange = "$$$",
                        ModelName = "LB-001-Tran",
                        Material = "Low Bridge",
                        Gender = "Unisex",
                        Color = "Transparent",
                        Details = "Perfect for Hipster and young people"
                    },

                    new Models.Frame {
                        Image = "lb1.png",
                        StoreLocation = "Optica Colonial",
                        PriceRange = "$",
                        ModelName = "LB-002-Rose",
                        Material = "Low Bridge",
                        Gender = "Women's",
                        Color = "Rose",
                        Details = "Medium to larger size, ideal for people with medium-size faces."
                    },

                    new Models.Frame {
                        Image = "lb2.png",
                        StoreLocation = "Opticentro Soler",
                        PriceRange = "$$$",
                        ModelName = "LB-003-RoseTran",
                        Material = "Low Bridge",
                        Gender = "Women's",
                        Color = "Transparent/Rose",
                        Details = "Designed in Italy, vintage look"
                    },

            };
        }


        //protected override async void OnAppearing()
        //{
        //    base.OnAppearing();

        //    // Populates listview from Database, works only with listView, not PancakeView
        //    //framesList.ItemsSource = await App.Database.GetFramesAsync();

        //}

        // Opens the individual frame in details page - FrameSelected
        private async void FrameSelected(object sender, EventArgs e)
        {
            var property = (sender as View).BindingContext as Models.Frame;
            await this.Navigation.PushAsync(new DetailsPage(property));
        }


        // User searches for frames in catalog of frames - DB not implemented
        private void SearchResult(object sender, EventArgs e)
        {
            // Needs implementation of search
        }

        // Adds a frame to the DB - not implemented
        private async void AddFrameButton(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync("AboutPage");
        }

        // Removes a frame from DB, first by searching for it - not implemented 
        private void RemoveFrameButton(object sender, EventArgs e)
        {
            //await Shell.Current.Navigation.PopAsync(animated: false);
        }

    }

}